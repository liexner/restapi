const mysql = require('mysql')


//Amazon database
const con = mysql.createConnection({
  host: "phonesessioninst.chr9b4wctai4.eu-central-1.rds.amazonaws.com",
  user: "masteradmin",
  password: "korv12POTATIS",
  database: "phoneSession",
})
/*
//google database
var con = mysql.createConnection({
  host: "35.228.21.5", 
  user: "wappsAdmin", 
  password: "korv12POTATIS", 
  database: "wapps", })
*/
con.connect((err) => {
  if (err) throw err
  console.log("Connected to database")
})

con.query(`CREATE TABLE IF NOT EXISTS users(
  user_id INT AUTO_INCREMENT PRIMARY KEY,
  password VARCHAR(100) NOT NULL,
  email VARCHAR(100) UNIQUE NOT NULL
)`)

con.query(`CREATE TABLE IF NOT EXISTS devices(
  device_id INT AUTO_INCREMENT PRIMARY KEY,
  user_id INT NOT NULL,
  device_name VARCHAR(30) NOT NULL,
  CONSTRAINT FK_devices_users FOREIGN KEY (user_id) REFERENCES users(user_id) ON DELETE CASCADE,
  CONSTRAINT id_name UNIQUE (user_id, device_name)
)`)

con.query(`CREATE TABLE IF NOT EXISTS sessions(
  session_id INT AUTO_INCREMENT PRIMARY KEY,
  device_id INT NOT NULL,
  app_name VARCHAR(80) NOT NULL,
  session_start DATETIME NOT NULL,
  session_end DATETIME NOT NULL,
  CONSTRAINT FK_sessions_devices FOREIGN KEY (device_id) REFERENCES devices(device_id) ON DELETE CASCADE
)`)

exports.verifyUser = (email, callback) => {
  const qry = `
  SELECT * 
  FROM users 
  WHERE email = (?)`
  const values = [email]
  con.query(qry, values, (error, result) => {
    callback(error, result)
  })
}

exports.createUser =  (email, password, callback) => {
  const qry = `
  INSERT INTO users (email, password) 
  VALUES (?, ?)`
  const values = [email, password]
  con.query(qry, values,  (error, result) => {
    callback(error, result)
  })
}

exports.deleteUser = (userId, callback) => {
  const qry = `
  DELETE FROM users 
  WHERE user_id = (?)`
  const values = [userId]

  con.query(qry, values, (error, result) => {
    callback(error, result)
  })
}

exports.createDevice = (userId, deviceName, callback) => {
  const qry = `
  INSERT INTO devices (user_id,device_name) 
  VALUES (?, ?)`
  const values = [userId, deviceName]
  con.query(qry, values,  (error, result) => {
    callback(error, result)
  })
}

exports.showAllDevice = (userId, callback) => {
  const qry = `
  SELECT devices.user_id, devices.device_id, devices.device_name
  FROM devices 
  INNER JOIN users ON users.user_id = devices.user_id
  WHERE users.user_id = (?)`
  const values = [userId]

  con.query(qry, values, (error, result) => {
    callback(error, result)
  })
}

exports.getUserId = (email, callback) => {
  const qry = `SELECT * 
  FROM users 
  WHERE email = (?)`
  const values = [email]

  con.query(qry, values, (error, result) => {

    callback(error, result)
  })
}

exports.deleteDevice = (deviceId, userId, callback) => {
  const qry = `DELETE FROM devices 
  WHERE devices.device_id = (?) AND devices.user_id = (?)`
  const values = [deviceId, userId]

  con.query(qry, values, (error, result) => {
    callback(error, result)
  })
}

exports.updateDevice = (userId, deviceName, deviceId, callback) => {
  const qry = `
  UPDATE devices 
  SET user_id = (?), device_name = (?), device_id = (?)
  WHERE device_id = (?) AND user_id = (?)`
  const values = [userId, deviceName, deviceId, deviceId, userId]

  con.query(qry, values, (error, result) => {
    callback(error, result)
  })
}

exports.updatePassword = (userId, newPassword, callback) => {
  const qry = `
  UPDATE users 
  SET password = (?)
  WHERE user_id = (?)
  `
  const values = [newPassword, userId]

  con.query(qry, values, (error, result) => {
    callback(error, result)
  })
}

exports.createSession = (deviceId, appName, sessionStart, sessionEnd, userID, callback) => {
  const qry = `
  SELECT user_id, device_id
  FROM devices
  WHERE user_id = (?) AND device_id = (?)`
  const values = [userID, deviceId]

  con.query(qry, values, (error, result) => {

    if (result === undefined || result.length == 0) {
      callback(error, result)
    } else {
      const qry2 = `
      INSERT INTO sessions (device_id, app_name, session_start, session_end)
      VALUES (?, ?, ?, ?)`
      const values2 = [deviceId, appName, sessionStart, sessionEnd]

      con.query(qry2, values2, (error, result) => {
        callback(error, result)
      })
    }
  })
}

exports.showAllSessions = (deviceId, userId, callback) => {
  const qry = `
  SELECT sessions.device_id, session_id, app_name, session_start, session_end
  FROM sessions
  INNER JOIN devices ON devices.device_id = sessions.device_id 
  WHERE devices.device_id = (?) AND devices.user_id = (?)`
  const values = [deviceId, userId]

  con.query(qry, values, (error, result) => {
    callback(error, result)
  })
}