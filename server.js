const express = require('express')
const formData = require("express-form-data")
const os = require("os")
const bodyPraser = require('body-parser')
const jwt = require('jsonwebtoken')
const bcrypt = require("bcrypt")
const request = require("request")
var randomstring = require("randomstring");
const config = require('./config')
const db = require('./database')
const aws = require("./amazon")
const google = require("./google")
const PORT = process.env.PORT || config.port

const app = express()
app.use(bodyPraser.json())

//handle file send to api -> send to go
//only use for upload to google bucket
app.use(formData.parse({uploadDir: os.tmpdir(),autoClean: true}));
app.use(formData.format());
app.use(formData.union());



app.use((req, res, next) => {
  console.log("Request: " + req.method + " " + req.originalUrl)
  next()
})

app.get("/res/google", (req, res) => {

  const code = req.query.code

  request.post(
    'https://www.googleapis.com/oauth2/v4/token', {
      form: {
        code: code,
        client_id: config.client_id,
        client_secret: config.client_secret,
        redirect_uri: config.redirect_uri,
        grant_type: "authorization_code"
      }
    },
    (error, response, body) => {

      if (!error && response.statusCode == 200) {
        const bodyParsed = JSON.parse(body)
        const idToken = bodyParsed.id_token
        const email = jwt.decode(idToken, {
          complete: true
        }).payload.email


        bcrypt.hash(randomstring.generate(), config.saltRounds, (err, hash) => {
          db.createUser(email, hash, (error) => {

            if (error) {
              if (error.errno == 1062) {
                db.getUserId(email, (error, result) => {
                  signAndSendToken(res, result[0].email, result[0].user_id)
                })
              } else {
                res.status(500).json({
                  error: "The request could not be performed"
                })
              }
            } else {
              db.getUserId(email, (error, result) => {
                signAndSendToken(res, result[0].email, result[0].user_id)
              })
            }
          })
        });
      }
    }
  );

})
app.put("/user", verifyToken, (req, res) => {
  const userId = req.user_id
  const newPassword = req.body.password

  bcrypt.hash(newPassword, config.saltRounds, (err, hash) => {

    db.updatePassword(userId, hash, (error, result) => {
      if (!error) {
        res.status(200).end()

      } else {
        console.log(error)
        res.status(500).json("The request could not be performed")
      }
    })
  })
})

app.get("/auth/google", (req, res) => {
  res.redirect("https://accounts.google.com/o/oauth2/v2/auth?client_id=" + config.client_id + "&redirect_uri=" + config.redirect_uri + "&response_type=code&scope=openid%20email&")
})

app.post("/token", (req, res) => {
  const grantType = req.body.grant_type
  const email = req.body.email
  const password = req.body.password

  if (grantType != "password") {
    res.status(400).json({
      error: "Unsupported grant type"
    })
    return
  }
  db.verifyUser(email, (error, result) => {
    if (error) {
      res.status(500).json({
        error: "The request could not be performed"
      })
    } else if (result === undefined || result.length == 0) {
      res.status(404).json({
        error: "User does not exist"
      })
    } else {
      bcrypt.compare(password, result[0].password, (err, compareResult) => {
        if (compareResult) {
          signAndSendToken(res, result[0].email, result[0].user_id)

        } else {
          res.status(403).json({
            error: "Wrong password"
          })
        }
      })
    }
  })
})
/*
//Upload to google bucket
app.post("/upload", verifyToken, (req, res) => {
  const image = req.files.picture
  
  google.fetchBucket((bucket) =>{
    if (bucket.id != config.googlebucket){
      res.status(404).json("Bucket not found")
    }
    else {
      google.uploadFile(bucket,image, (error,file) =>{
      if(error){
        res.status(500).json("The request could not be performed")
      }
      else{
        res.status(200).end()
      }
    })   
    }
  })
})*/


//Upload to amazon bucket
app.post("/upload", verifyToken, (req, res) => {

  aws.uploadimage((error, data) => {
    if (error) {
      res.status(500).josn("Communication failed with Amazon S3")
    } else if (data) {
      res.status(200).json(data)
    }
  })
})

app.post('/user', (req, res) => {
  const email = req.body.email
  const password = req.body.password

  bcrypt.hash(password, config.saltRounds, (err, hash) => {
    db.createUser(email, hash, (error, result) => {
      if (error) {
        if (error.errno == 1062) {
          res.status(409).json({
            error: "User alredy exsists"
          })
        } else {
          res.status(500).json({
            error: "The request could not be performed"
          })
        }
      } else {
        res.status(200).end()
      }
    })
  })
})

app.delete("/user", verifyToken, (req, res) => {
  const userID = req.user_id

  db.deleteUser(userID, (error, results) => {
    if (error) {
      res.status(500).json("The request could not be performed")
    } else {
      res.status(204).end()
    }
  })
})

app.get("/devices", verifyToken, (req, res) => {
  const userId = req.user_id

  db.showAllDevice(userId, (error, result) => {
    if (error) {
      res.status(500).json("The request could not be performed")
    } else if (result === undefined || result.length == 0) {
      res.status(404).json("Device(s) not found")
    } else {
      res.status(200).json(result)
    }
  })
})

app.post("/devices", verifyToken, (req, res) => {
  const userId = req.user_id
  const deviceName = req.body.device_name

  db.createDevice(userId, deviceName, (error, result) => {
    if (error) {
      if (error.errno == 1062) {
        res.status(409).json("Device with that name already exists")
      } else {
        res.status(500).json("The request could not be performed")
      }
    } else {
      res.status(200).end()
    }
  })
})

app.delete("/devices/:device_id", verifyToken, (req, res) => {
  const deviceId = req.params.device_id
  const userID = req.user_id

  db.deleteDevice(deviceId, userID, (error, result) => {
    if (error) {
      res.status(500).json("The request could not be performed")
    } else {
      res.status(204).end()
    }
  })
})

app.put("/devices", verifyToken, (req, res) => {
  const userId = req.user_id
  const deviceName = req.body.device_name
  const deviceID = req.body.device_id

  db.updateDevice(userId, deviceName, deviceID, (error, result) => {
    if (error) {
      if (error.errno == 1062) {
        res.status(409).json("Device with that name already exists")
      } else {
        res.status(500).json("The request could not be performed")
      }
    } else {
      res.status(200).end()
    }
  })
})

app.post("/sessions", verifyToken, (req, res) => {
  const userID = req.user_id
  const deviceId = req.body.device_id
  const appName = req.body.app_name
  const sessionStart = req.body.session_start
  const sessionEnd = req.body.session_end

  db.createSession(deviceId, appName, sessionStart, sessionEnd, userID, (error, result) => {

    if (error) {
      res.status(500).json("The request could not be performed")
    } else if (result === undefined || result.length == 0) {
      res.status(403).json("user_id did not match with the user_id in the device table for the given device_id")
    } else {
      res.status(201).end()
    }
  })
})

app.get("/sessions/:device_id", verifyToken, (req, res) => {
  const deviceId = req.params.device_id
  const userId = req.user_id

  db.showAllSessions(deviceId, userId, (error, result) => {
    if (error) {
      res.status(500).json("The request could not be performed")
    } else {
      res.status(200).json(result)
    }
  })
})

app.listen(PORT, () => {
  console.log('Server started on: ' + PORT);
});

function signAndSendToken(res, email, user_id) {

  var id_token = jwt.sign({
    user_id,
    email,
  }, config.secret, {
    expiresIn: '1h'
  })
  var access_token = jwt.sign({
    user_id,
  }, config.secret, {
    expiresIn: '1h'
  })

  res.status(200).json({
    id_token,
    access_token
  })
}


function verifyToken(req, res, next) {
  const bearerHeader = req.headers["authorization"]

  if (typeof bearerHeader !== "undefined") {
    const bearer = bearerHeader.split(" ")
    const bearerToken = bearer[1]

    jwt.verify(bearerToken, config.secret, (err, authData) => {
      if (err) {
        res.status(403).end()
      } else {
        req.user_id = authData.user_id
        next()
      }
    })
  } else {
    res.status(403).end()
  }
}