const {Storage} = require("@google-cloud/storage")
const config = require("./config")

const storage = new Storage()

exports.uploadFile = (bucket, file, callback) =>{
    const stream = bucket.file(file.name).createWriteStream({
        metadata: {
            contentType: file.mimetype
        },
        resumable: false
    })

    bucket.upload(file.path, {}, (error,file) => {
        callback(error,file)  
    })

    
}

exports.fetchBucket = (callback) => {
    const bucket = storage.bucket(config.googlebucket)
    callback(bucket)
}